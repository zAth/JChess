package me.zath.jchess.screen;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.zath.jchess.JChess;
import me.zath.jchess.board.Board;
import me.zath.jchess.board.Tile;
import me.zath.jchess.button.AbstractButton;
import me.zath.jchess.button.NewGameButton;
import me.zath.jchess.input.GameInputProcessor;
import me.zath.jchess.pieces.AbstractPiece;

import java.util.ArrayList;
import java.util.List;

public class GameScreen extends AbstractScreen {

    private SpriteBatch spriteBatch;
    private BitmapFont bitmapFont;
    private GameInputProcessor gameInputProcessor;

    private Board board;
    private List<AbstractPiece> fallenPieces;
    private AbstractPiece.Color turn;
    private AbstractPiece.Color winner;

    public GameScreen(JChess jChess) {
        super(jChess);

        fallenPieces = new ArrayList<>();
        turn = AbstractPiece.Color.WHITE;

        spriteBatch = new SpriteBatch();
        bitmapFont = new BitmapFont();
        board = new Board();
        gameInputProcessor = new GameInputProcessor(this);

        List<AbstractButton> buttonList = new ArrayList<>();
        buttonList.add(new NewGameButton("Novo Jogo", getHeight() + (getSide() / 10), (getHeight() * 2) / 5
            , getHeight() / 5, getHeight() / 5)); // quit button
        buttonList.add(new NewGameButton("Novo Jogo", getWidth() - (getSide() / 10) - (getHeight() / 5), (getHeight() * 2) / 5
            , getHeight() / 5, getHeight() / 5)); // undo button

        jChess.getButtonManager().setButtons(buttonList);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gameInputProcessor);
    }

    @Override
    public void render(float delta) {
        spriteBatch.begin();

        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Tile tile = board.getTile(x, y);
                Texture tileTexture = tile.getColor() == AbstractPiece.Color.WHITE ? getjChess().getAssetManager().getTexture("whiteTile")
                    : getjChess().getAssetManager().getTexture("blackTile");

                if (gameInputProcessor.getPiece() != null) {
                    if (gameInputProcessor.getPiece().isValidPath(tile)) {
                        if (tile.getPiece() != null) {
                            if (tile.getPiece().getColor() != gameInputProcessor.getPiece().getColor()) {
                                tileTexture = getjChess().getAssetManager().getTexture("redTile");
                            }
                        } else {
                            tileTexture = getjChess().getAssetManager().getTexture("greenTile");
                        }
                    }

                    if (gameInputProcessor.getPiece().getTile() == tile)
                        tileTexture = getjChess().getAssetManager().getTexture("yellowTile");
                }


                spriteBatch.draw(tileTexture, tile.getX() * getTile_size(), tile.getY() * getTile_size()
                    , getTile_size(), getTile_size());

                if (tile.getPiece() != null) {
                    String pieceName = getjChess().getAssetManager().getPieceName(tile.getPiece());
                    Texture pieceTexture = getjChess().getAssetManager().getTexture(pieceName);
                    spriteBatch.draw(pieceTexture, tile.getX() * getTile_size(), tile.getY() * getTile_size(), getTile_size(), getTile_size());
                }
            }
        }

        Texture turnTexture = turn == AbstractPiece.Color.WHITE ? getjChess().getAssetManager().getTexture("whiteTile")
            : getjChess().getAssetManager().getTexture("blackTile");
        spriteBatch.draw(turnTexture, getHeight() + (getSide() / 20), (getHeight()) * 3 / 8, getSide() - (getSide() / 10), getHeight() / 4);

        getjChess().getButtonManager().getButtons().forEach(button -> {
                spriteBatch.draw(getjChess().getAssetManager().getTexture("roundSquare"), button.getX(), button.getY()
                    , button.getWidth(), button.getHeigth());
                bitmapFont.draw(spriteBatch, button.getDisplayText()
                    , button.getX() + button.getWidth() / 2 - 5 * button.getDisplayText().length()
                    , button.getY() + button.getHeigth() / 2);
            }
        );

        int fallenBlackPiecesRendered = 0;
        int fallenWhitePiecesRendered = 0;

        for(AbstractPiece fallenPiece : fallenPieces){
            String pieceName = getjChess().getAssetManager().getPieceName(fallenPiece);
            Texture pieceTexture = getjChess().getAssetManager().getTexture(pieceName);

            int x;
            int y;
            if(fallenPiece.getColor() == AbstractPiece.Color.BLACK){
                x = (int) (getHeight() + (getSide() / 10) + (fallenBlackPiecesRendered * getSide() / 12) - ((fallenBlackPiecesRendered / 5) * 4 * getTile_size() / 2));
                y = (int) (getHeight() - (getHeight() / 10) - (getTile_size() / 2 * (fallenBlackPiecesRendered / 5)));
                fallenBlackPiecesRendered++;
            } else {
                x = (int) (getHeight() + (getSide() / 10) + (fallenWhitePiecesRendered * getSide() / 12) - ((fallenWhitePiecesRendered / 5) * 4 * getTile_size() / 2));
                y = (int) ((getHeight() / 10) + (getTile_size() / 2 * (fallenWhitePiecesRendered / 5)));
                fallenWhitePiecesRendered++;
            }

            spriteBatch.draw(pieceTexture, x, y, getTile_size() / 2, getTile_size() / 2);
        }

        spriteBatch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public GameInputProcessor getGameInputProcessor() {
        return gameInputProcessor;
    }

    public List<AbstractPiece> getFallenPieces() {
        return fallenPieces;
    }

    public AbstractPiece.Color getTurn() {
        return turn;
    }

    public void setTurn(AbstractPiece.Color turn) {
        this.turn = turn;
    }

    public AbstractPiece.Color getWinner() {
        return winner;
    }

    public void setWinner(AbstractPiece.Color winner) {
        this.winner = winner;
    }

}
