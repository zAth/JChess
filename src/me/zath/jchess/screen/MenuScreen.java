package me.zath.jchess.screen;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.zath.jchess.JChess;
import me.zath.jchess.button.AbstractButton;
import me.zath.jchess.button.SinglePlayerButton;
import me.zath.jchess.input.InputProcessor;

import java.util.ArrayList;
import java.util.List;

public class MenuScreen extends AbstractScreen {

    private SpriteBatch spriteBatch;
    private BitmapFont bitmapFont;
    private InputProcessor inputProcessor;


    public MenuScreen(JChess jChess) {
        super(jChess);

        spriteBatch = new SpriteBatch();
        bitmapFont = new BitmapFont();
        inputProcessor = new InputProcessor(this);

        List<AbstractButton> buttonList = new ArrayList<>();
        // todo nickname
        buttonList.add(new SinglePlayerButton("Um jogador"
            , (getWidth() - (getWidth() * 4 / 5)) / 2
            , getHeight() - (getHeight() * 2 / 25) - (getHeight() * 2 / 5)
            , getWidth() * 4 / 5
            , getHeight() / 5)); // singlePlayer button
        buttonList.add(new SinglePlayerButton("Dois jogadores"
            , (getWidth() - (getWidth() * 4 / 5)) / 2
            , getHeight() - (getHeight() * 3 / 25) - (getHeight() * 3 / 5)
            , getWidth() * 4 / 5
            , getHeight() / 5)); // local multiPlayer button
        buttonList.add(new SinglePlayerButton("Servidor"
            , (getWidth() - (getWidth() * 4 / 5)) / 2
            , getHeight() / 25
            , getWidth() * 4 / 5
            , getHeight() / 5)); // server multiPlayer button

        jChess.getButtonManager().setButtons(buttonList);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputProcessor);
    }

    @Override
    public void render(float v) {
        spriteBatch.begin();

        getjChess().getButtonManager().getButtons().forEach(button -> {
                spriteBatch.draw(getjChess().getAssetManager().getTexture("roundSquare"), button.getX(), button.getY()
                    , button.getWidth(), button.getHeigth());
                bitmapFont.draw(spriteBatch, button.getDisplayText()
                    , button.getX() + button.getWidth() / 2 - 5 * button.getDisplayText().length()
                    , button.getY() + button.getHeigth() / 2);
            }
        );

        spriteBatch.end();
    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
