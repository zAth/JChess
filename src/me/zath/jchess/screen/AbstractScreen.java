package me.zath.jchess.screen;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.Screen;
import me.zath.jchess.JChess;

import java.awt.*;

public class AbstractScreen implements Screen {

    private JChess jChess;
    private int height;
    private int side;
    private int width;
    private float tile_size;

    public AbstractScreen(JChess jChess) {
        this.jChess = jChess;
        height = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);
        side = getHeight() * 2 / 3;
        width = getHeight() + getSide();
        tile_size = getHeight() / 8;
    }

    public JChess getjChess() {
        return jChess;
    }

    public int getHeight() {
        return height;
    }

    public int getSide() {
        return side;
    }

    public int getWidth() {
        return width;
    }

    public float getTile_size() {
        return tile_size;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float v) {

    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
