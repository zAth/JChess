package me.zath.jchess.input;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.Input;
import me.zath.jchess.JChess;
import me.zath.jchess.board.Tile;
import me.zath.jchess.button.AbstractButton;
import me.zath.jchess.pieces.AbstractPiece;
import me.zath.jchess.screen.GameScreen;

public class GameInputProcessor extends InputProcessor {

    private GameScreen gameScreen;
    private AbstractPiece piece;

    public GameInputProcessor(GameScreen gameScreen) {
        super(gameScreen);
        this.gameScreen = gameScreen;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        int realY = gameScreen.getHeight() - screenY; // y axis is flipped
        System.out.println("X:" + screenX + ", Y:" + realY);
        if (button == Input.Buttons.LEFT) {
            if (screenX < gameScreen.getHeight()) {
                int x = (int) (screenX / gameScreen.getTile_size());
                int y = (int) (realY / gameScreen.getTile_size());
                Tile tile = gameScreen.getBoard().getTile(x, y);

                if (gameScreen.getWinner() == null) {
                    if (piece != null) {
                        if (piece.getColor() == gameScreen.getTurn()) {
                            if (piece.move(tile)) {
                                piece = null;
                                gameScreen.setTurn(gameScreen.getTurn() == AbstractPiece.Color.WHITE
                                        ? AbstractPiece.Color.BLACK
                                        : AbstractPiece.Color.WHITE
                                );
                                // todo check if is ai turn(black), make it move and then change turn again
                            } else if (tile.getPiece() != null) {
                                piece = tile.getPiece();
                            }
                        } else if (tile.getPiece() != null) {
                            piece = tile.getPiece();
                        }
                    } else {
                        piece = tile.getPiece();
                    }
                }
            } else {
                AbstractButton clickedButton = JChess.getJChess().getButtonManager().getButtons().stream()
                    .filter(abstractButton -> screenX >= abstractButton.getX() && screenX <= abstractButton.getX() + abstractButton.getWidth())
                    .filter(abstractButton -> screenY >= abstractButton.getY() && screenY <= abstractButton.getY() + abstractButton.getHeigth())
                    .findFirst()
                    .orElse(null);

                if (clickedButton != null)
                    clickedButton.click();
            }
        } else if (button == Input.Buttons.RIGHT) {
            piece = null;
        }

        return false;
    }

    public AbstractPiece getPiece() {
        return piece;
    }

    public void setPiece(AbstractPiece piece) {
        this.piece = piece;
    }

}
