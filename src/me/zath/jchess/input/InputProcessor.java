package me.zath.jchess.input;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.InputAdapter;
import me.zath.jchess.JChess;
import me.zath.jchess.animation.Hoverable;
import me.zath.jchess.animation.ZoomInAnimation;
import me.zath.jchess.animation.ZoomOutAnimation;
import me.zath.jchess.button.AbstractButton;
import me.zath.jchess.screen.AbstractScreen;

public class InputProcessor extends InputAdapter {

    private AbstractScreen abstractScreen;

    //hover
    private Hoverable currentHovered;
    // todo make tiles and pieces implement hoverable interface? so all of them zoom when hovered
    //   they will need to have extra vars for x and y since their coordinates go from 0 to 7 and not 0 to 1000

    public InputProcessor(AbstractScreen abstractScreen) {
        this.abstractScreen = abstractScreen;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        int y = abstractScreen.getHeight() - screenY; // y axis is flipped
        Hoverable hovered = JChess.getJChess().getButtonManager().getButtons().stream()
            .filter(button -> screenX > button.getX() && screenX < button.getX() + button.getWidth())
            .filter(button -> y > button.getY() && y < button.getY() + button.getHeigth())
            .findFirst()
            .orElse(null);

        if (currentHovered != null) {
            if (hovered == null) {
                new ZoomOutAnimation(2, (float) 0.02, currentHovered);
                currentHovered = null;
            } else {
                if (currentHovered != hovered) {
                    new ZoomOutAnimation(2, (float) 0.02, currentHovered);
                    new ZoomInAnimation(2, (float) 0.02, hovered);
                    currentHovered = hovered;
                }
            }
        } else {
            if (hovered != null) {
                new ZoomInAnimation(2, (float) 0.02, hovered);
                currentHovered = hovered;
            }
        }

        return super.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        int y = getAbstractScreen().getHeight() - screenY; // y axis is flipped
        AbstractButton clickedButton = JChess.getJChess().getButtonManager().getButtons().stream()
            .filter(abstractButton -> screenX >= abstractButton.getX() && screenX <= abstractButton.getX() + abstractButton.getWidth())
            .filter(abstractButton -> y >= abstractButton.getY() && y <= abstractButton.getY() + abstractButton.getHeigth())
            .findFirst()
            .orElse(null);

        if (clickedButton != null)
            clickedButton.click();

        return false;
    }

    public AbstractScreen getAbstractScreen() {
        return abstractScreen;
    }
}
