package me.zath.jchess.util;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;

public enum  Direction {

    NORTH(0, 1),
    NORTHEAST(1, 1),
    EAST(1, 0),
    SOUTHEAST(1, -1),
    SOUTH(0, -1),
    SOUTHWEST(-1, -1),
    WEST(-1, 0),
    NORTHWEST(-1, 1),
    STATIC(0, 0);

    private int x, y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection(Tile from, Tile to){
        if(to.getX() > from.getX()){
            if(to.getY() > from.getY()){
                return NORTHEAST;
            } else if(to.getY() < from.getY()){
                return SOUTHEAST;
            } else if(to.getY() == from.getY()){
                return EAST;
            }
        } else if(to.getX() < from.getX()){
            if(to.getY() > from.getY()){
                return NORTHWEST;
            } else if(to.getY() < from.getY()){
                return SOUTHWEST;
            } else if(to.getY() == from.getY()){
                return WEST;
            }
        } else if(to.getY() > from.getY()){
            if(to.getX() > from.getX()){
                return NORTHEAST;
            } else if(to.getX() < from.getX()){
                return NORTHWEST;
            } else if(to.getX() == from.getX()){
                return NORTH;
            }
        } else if(to.getY() < from.getY()){
            if(to.getX() > from.getX()){
                return SOUTHEAST;
            } else if(to.getX() < from.getX()){
                return SOUTHWEST;
            } else if(to.getX() == from.getX()){
                return SOUTH;
            }
        }

        return STATIC;
    }

}
