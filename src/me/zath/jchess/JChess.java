package me.zath.jchess;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import me.zath.jchess.animation.AbstractAnimation;
import me.zath.jchess.animation.AnimationManager;
import me.zath.jchess.button.ButtonManager;
import me.zath.jchess.managers.AssetManager;
import me.zath.jchess.screen.AbstractScreen;
import me.zath.jchess.screen.MenuScreen;

import java.util.ArrayList;
import java.util.List;

public class JChess extends Game {

    // todo
    // gamescreen render coordinates arent the same of all the other coordinates
    // the tiles are set with proper coordinates, same as screen, inputProcessor listens to the right coordinates,
    //   render calcules prints the right coordinates but then it draws on the wrong pixel
    //   does it have to do with the resize method here?

    // side gui                                                                              1
    //   player names on game screen
    //   history of moves
    //   button to undo
    //     if used in server multiplayer, create a request to the opponent so he can accept or deny the undo
    //   button to quit
    //     replace the new game button
    //     get back to server screen if playing server multiplayer
    //     get back to menu screen if playing singleplayer or local multiplayer

    // rotate board? it will need a hard change on each piece coordinate calcules            2

    // beautify                                                                              3
    //   assets
    //   animations
    //     change zoom amount to a percentage of the game dimension
    //     change font size to a percentage of the game dimension
    //     zoom display text aswell on zoom animations
    //     more animations?
    //   animate pieces
    //     hovered
    //     selected
    //     moving
    //     dying
    //   remove game border?

    // menu screen                                                                           4
    //   insert nickname
    //   singleplayer ( ai )
    //   local multiplayer ( current )
    //   multiplayer ( server )

    // multiplayer ( server ) screen                                                         5
    //   current rooms
    //   button to create a room
    //   button to get back to menu screen

    // server                                                                                6
    //   validates all requested moves by the 2 clients
    //   copy:
    //     board
    //     tile
    //     direction
    //   room object
    //     white pieces player, who creates the room
    //     black pieces player, who joins the room
    //     list of spectatores

    // ai                                                                                    7

    // remove libgdx dependency                                                              8

    private static JChess jchess;

    private ButtonManager buttonManager;
    private AssetManager assetManager;
    private AnimationManager animationManager;

    private AbstractScreen abstractScreen;

    public JChess() {
        super();
        jchess = this;
    }

    @Override
    public void create() {
        buttonManager = new ButtonManager();
        assetManager = new AssetManager();
        animationManager = new AnimationManager();

        abstractScreen = new MenuScreen(this);

        this.setScreen(abstractScreen);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        List<AbstractAnimation> finishedAnimations = new ArrayList<>();
        animationManager.getAnimations().forEach(abstractAnimation -> {
            if(abstractAnimation.isFinished())
                finishedAnimations.add(abstractAnimation);
            else
                abstractAnimation.tick(Gdx.graphics.getDeltaTime());
        });
        finishedAnimations.forEach(abstractAnimation -> animationManager.getAnimations().remove(abstractAnimation));

        super.render();
    }

    public static JChess getJChess() {
        return jchess;
    }

    public AbstractScreen getAbstractScreen() {
        return abstractScreen;
    }

    public ButtonManager getButtonManager() {
        return buttonManager;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public AnimationManager getAnimationManager() {
        return animationManager;
    }

    public void setAbstractScreen(AbstractScreen abstractScreen) {
        this.abstractScreen = abstractScreen;
        this.setScreen(abstractScreen);
    }
}
