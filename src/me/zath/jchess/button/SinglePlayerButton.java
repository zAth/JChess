package me.zath.jchess.button;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.JChess;
import me.zath.jchess.animation.ZoomInOutAnimation;
import me.zath.jchess.screen.GameScreen;

public class SinglePlayerButton extends AbstractButton {

    public SinglePlayerButton(String displayText, int x, int y, int width, int heigth) {
        super(displayText, x, y, width, heigth);
    }

    @Override
    public void click() {
        new ZoomInOutAnimation(4, (float) 0.02, this);

        JChess jChess = JChess.getJChess();
        GameScreen gameScreen = new GameScreen(jChess);
        jChess.setAbstractScreen(gameScreen);
    }

}
