package me.zath.jchess.button;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.animation.Hoverable;

public abstract class AbstractButton implements Hoverable {

    private String displayText;
    private int x;
    private int y;
    private int width;
    private int heigth;

    public AbstractButton(String displayText, int x, int y, int width, int heigth) {
        this.displayText = displayText;
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
    }

    public abstract void click();

    public String getDisplayText() {
        return displayText;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

}
