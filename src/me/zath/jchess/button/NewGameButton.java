package me.zath.jchess.button;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.JChess;
import me.zath.jchess.animation.ZoomInOutAnimation;
import me.zath.jchess.board.Board;
import me.zath.jchess.pieces.AbstractPiece;
import me.zath.jchess.screen.GameScreen;

public class NewGameButton extends AbstractButton {

    public NewGameButton(String displayText, int x, int y, int width, int heigth) {
        super(displayText, x, y, width, heigth);
    }

    @Override
    public void click() {
        new ZoomInOutAnimation(4, (float) 0.02, this);

        GameScreen gameScreen = ((GameScreen) JChess.getJChess().getAbstractScreen());
        gameScreen.setBoard(new Board());
        gameScreen.getGameInputProcessor().setPiece(null);
        gameScreen.getFallenPieces().clear();
        gameScreen.setTurn(AbstractPiece.Color.WHITE);
        gameScreen.setWinner(null);
    }

}
