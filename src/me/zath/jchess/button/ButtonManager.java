package me.zath.jchess.button;
/*
 * JChess 
 * Created by zAth
 */

import java.util.ArrayList;
import java.util.List;

public class ButtonManager {

    private List<AbstractButton> buttons;

    public ButtonManager() {
        this.buttons = new ArrayList<>();
    }

    public List<AbstractButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<AbstractButton> buttons) {
        this.buttons = buttons;
    }
}
