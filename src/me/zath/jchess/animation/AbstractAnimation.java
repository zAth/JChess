package me.zath.jchess.animation;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.JChess;

public abstract class AbstractAnimation {

    private int duration;
    private float delay;

    private float phaseCount;
    private int count;

    private boolean finished;

    /**
     * @param delay delay between frames,
     * @param duration number of times the delay will be reset
     * */
    public AbstractAnimation(int duration, float delay) {
        this.duration = duration;
        this.delay = delay;

        phaseCount = 0;
        count = 0;

        finished = false;

        JChess.getJChess().getAnimationManager().getAnimations().add(this);
    }

    public void tick(float alpha){
        phaseCount += alpha;

        if(phaseCount >= delay) {
            phaseCount = 0;
            count += 1;

            if (count > duration) {
                finished = true;
            }

            if (finished)
                return;

            animate();
        }
    }

    public int getCount() {
        return count;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isFinished() {
        return finished;
    }

    public abstract void animate();

}
