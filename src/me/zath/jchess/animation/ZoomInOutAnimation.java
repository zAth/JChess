package me.zath.jchess.animation;
/*
 * JChess 
 * Created by zAth
 */

public class ZoomInOutAnimation extends AbstractAnimation {

    private Hoverable hoverable;

    public ZoomInOutAnimation(int duration, float delay, Hoverable hoverable) {
        super(duration, delay);
        this.hoverable = hoverable;
    }

    @Override
    public void animate() {
        if(getCount() <= ((double) getDuration() / 2.0)){
            hoverable.setX(hoverable.getX() - 5);
            hoverable.setWidth(hoverable.getWidth() + 10);

            hoverable.setY(hoverable.getY() - 5);
            hoverable.setHeigth(hoverable.getHeigth() + 10);
        } else {
            hoverable.setX(hoverable.getX() + 5);
            hoverable.setWidth(hoverable.getWidth() - 10);

            hoverable.setY(hoverable.getY() + 5);
            hoverable.setHeigth(hoverable.getHeigth() - 10);
        }
    }

}
