package me.zath.jchess.animation;
/*
 * JChess 
 * Created by zAth
 */

public class ZoomInAnimation extends AbstractAnimation {

    private Hoverable hoverable;

    public ZoomInAnimation(int duration, float delay, Hoverable hoverable) {
        super(duration, delay);
        this.hoverable = hoverable;
    }

    @Override
    public void animate() {
        hoverable.setX(hoverable.getX() - 5);
        hoverable.setWidth(hoverable.getWidth() + 10);

        hoverable.setY(hoverable.getY() - 5);
        hoverable.setHeigth(hoverable.getHeigth() + 10);
    }

}
