package me.zath.jchess.animation;
/*
 * JChess 
 * Created by zAth
 */

public interface Hoverable {

    void setX(int x);
    void setWidth(int width);
    void setY(int y);
    void setHeigth(int heigth);
    int getX();
    int getWidth();
    int getY();
    int getHeigth();

}
