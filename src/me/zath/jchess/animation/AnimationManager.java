package me.zath.jchess.animation;
/*
 * JChess 
 * Created by zAth
 */

import java.util.ArrayList;
import java.util.List;

public class AnimationManager {

    private List<AbstractAnimation> animations;

    public AnimationManager() {
        this.animations = new ArrayList<>();
    }

    public List<AbstractAnimation> getAnimations() {
        return animations;
    }

}
