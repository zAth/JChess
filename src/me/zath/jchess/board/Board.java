package me.zath.jchess.board;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.pieces.*;

public class Board {

    private Tile[][] tiles;

    public Board() {
        tiles = new Tile[8][8];
        int[][] pieces = new int[][]{
            {4, 3, 2, 5, 6, 2, 3, 4},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {4, 3, 2, 5, 6, 2, 3, 4},
        };

        int colorCount = 0;
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (y % 2 == 0) {
                    if (x % 2 == 0) {
                        tiles[x][y] = new Tile(AbstractPiece.Color.BLACK, x, y, this);
                    } else {
                        tiles[x][y] = new Tile(AbstractPiece.Color.WHITE, x, y, this);
                    }
                } else {
                    if (x % 2 == 0) {
                        tiles[x][y] = new Tile(AbstractPiece.Color.WHITE, x, y, this);
                    } else {
                        tiles[x][y] = new Tile(AbstractPiece.Color.BLACK, x, y, this);
                    }
                }

                AbstractPiece.Color color = colorCount > 16 ? AbstractPiece.Color.WHITE : AbstractPiece.Color.BLACK;

                if(pieces[x][y] == 1){
                    tiles[x][y].setPiece(new Pawn(tiles[x][y], color));
                } else if(pieces[x][y] == 2){
                    tiles[x][y].setPiece(new Bishop(tiles[x][y], color));
                } else if(pieces[x][y] == 3){
                    tiles[x][y].setPiece(new Horse(tiles[x][y], color));
                } else if(pieces[x][y] == 4){
                    tiles[x][y].setPiece(new Tower(tiles[x][y], color));
                } else if(pieces[x][y] == 5){
                    tiles[x][y].setPiece(new Queen(tiles[x][y], color));
                } else if(pieces[x][y] == 6){
                    tiles[x][y].setPiece(new King(tiles[x][y], color));
                }

                colorCount++;
            }
        }
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public Tile getTile(int x, int y) {
        return tiles[x][y];
    }

    public String getAlgebraicNotation(Tile tile) {
        String toReturn = "";

        if (tile.getX() == 0) {
            toReturn = "a";
        } else if (tile.getX() == 1) {
            toReturn = "b";
        } else if (tile.getX() == 2) {
            toReturn = "c";
        } else if (tile.getX() == 3) {
            toReturn = "d";
        } else if (tile.getX() == 4) {
            toReturn = "e";
        } else if (tile.getX() == 5) {
            toReturn = "f";
        } else if (tile.getX() == 6) {
            toReturn = "g";
        } else if (tile.getX() == 7) {
            toReturn = "h";
        }

        toReturn += tile.getY() + 1;

        return toReturn;
    }

}
