package me.zath.jchess.board;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.pieces.AbstractPiece;

public class Tile {

    private AbstractPiece.Color color;
    private int x, y;
    private Board board;
    private AbstractPiece piece;

    public Tile(AbstractPiece.Color color, int x, int y, Board board) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.piece = null;
        this.board = board;
    }

    public AbstractPiece.Color getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Board getBoard() {
        return board;
    }

    public AbstractPiece getPiece() {
        return piece;
    }

    public void setPiece(AbstractPiece piece) {
        this.piece = piece;
    }

    @Override
    public String toString() {
        return "Tile{" +
            "color=" + color.name() +
            ", x=" + x +
            ", y=" + y +
            ", piece=" + (piece != null ? piece.toString() : "null") +
            '}';
    }

}
