package me.zath.jchess;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import java.awt.*;

public class Application {

    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();

        configuration.vSyncEnabled = true;
        configuration.title = "JChess";
        configuration.height = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2);
        configuration.width = configuration.height + (configuration.height * 2 / 3);
        configuration.resizable = false;

        LwjglApplication lwjglApplication = new LwjglApplication(new JChess(), configuration);
    }

}
