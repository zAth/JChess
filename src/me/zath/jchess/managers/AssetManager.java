package me.zath.jchess.managers;
/*
 * JChess 
 * Created by zAth
 */

import com.badlogic.gdx.graphics.Texture;
import me.zath.jchess.pieces.AbstractPiece;

import java.util.HashMap;

public class AssetManager {

    private HashMap<String, Texture> images;

    public AssetManager() {
        this.images = new HashMap<>();

        images.put("pawn_white", new Texture("assets/pawn_white.png"));
        images.put("pawn_black", new Texture("assets/pawn_black.png"));
        images.put("bishop_white", new Texture("assets/bishop_white.png"));
        images.put("bishop_black", new Texture("assets/bishop_black.png"));
        images.put("horse_white", new Texture("assets/horse_white.png"));
        images.put("horse_black", new Texture("assets/horse_black.png"));
        images.put("tower_white", new Texture("assets/tower_white.png"));
        images.put("tower_black", new Texture("assets/tower_black.png"));
        images.put("queen_white", new Texture("assets/queen_white.png"));
        images.put("queen_black", new Texture("assets/queen_black.png"));
        images.put("king_white", new Texture("assets/king_white.png"));
        images.put("king_black", new Texture("assets/king_black.png"));
        images.put("whiteTile", new Texture("assets/whiteTile.png"));
        images.put("blackTile", new Texture("assets/blackTile.png"));
        images.put("yellowTile", new Texture("assets/yellowTile.png"));
        images.put("redTile", new Texture("assets/redTile.png"));
        images.put("greenTile", new Texture("assets/greenTile.png"));
        images.put("roundSquare", new Texture("assets/roundSquare.png"));
    }

    public Texture getTexture(String name) {
        return images.get(name);
    }

    public String getPieceName(AbstractPiece abstractPiece) {
        return abstractPiece.getClass().getSimpleName().toLowerCase() + "_" + abstractPiece.getColor().name().toLowerCase();
    }

}
