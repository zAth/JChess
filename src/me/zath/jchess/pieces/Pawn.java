package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;

public class Pawn extends AbstractPiece {

    // todo en passant

    private boolean moved;

    public Pawn(Tile tile, Color color) {
        super(tile, color);
        moved = false;
    }

    @Override
    public boolean move(Tile tile) {
        boolean superMoved = super.move(tile);
        if (superMoved) {
            int endX = color == Color.BLACK ? 7 : 0;
            if (tile.getX() == endX)
                new Queen(tile, color);
            else
                moved = true;
        }

        return superMoved;
    }

    @Override
    public boolean isValidPath(Tile tile) {
        if ((color == Color.BLACK && this.tile.getX() > tile.getX())
            || (color == Color.WHITE && this.tile.getX() < tile.getX())) { // cant go back
            return false;
        }

        int dirCount = color == Color.BLACK ? 1 : -1;
        if (this.tile.getY() == tile.getY()) { // if is only moving
            if (this.tile.getBoard().getTile(this.tile.getX() + dirCount, this.tile.getY()).getPiece() != null) { // if has some piece blocking
                return false;
            }
            if (tile.getPiece() != null) { // if has some piece in the destination tile
                return false;
            }
            if (!moved) {
                if (color == Color.BLACK && this.tile.getX() < tile.getX() - (dirCount * 2)) { // can move 2 tiles if not moved
                    return false;
                }
                if (color == Color.WHITE && this.tile.getX() > tile.getX() - (dirCount * 2)) { // can move 2 tiles if not moved
                    return false;
                }
            } else {
                if (this.tile.getX() != tile.getX() - dirCount) { // can only move 1 tile
                    return false;
                }
            }
        } else if (this.tile.getY() - tile.getY() == -1 || this.tile.getY() - tile.getY() == 1) { // if is attacking
            if (this.tile.getX() != tile.getX() - dirCount) { // can only attack 1 tile diagonally
                return false;
            }
            if (tile.getPiece() == null) { // can only attack if has 1 piece there
                return false;
            }
            return tile.getPiece().getColor() != getColor();
        } else {
            return false;
        }

        return true;
    }

}
