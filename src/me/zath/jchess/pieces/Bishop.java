package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;
import me.zath.jchess.util.Direction;

public class Bishop extends AbstractPiece {

    public Bishop(Tile tile, Color color) {
        super(tile, color);
    }

    @Override
    public boolean isValidPath(Tile tile) {
        int xDiff = tile.getX() - this.tile.getX() < 0 ? (tile.getX() - this.tile.getX()) * -1 : tile.getX() - this.tile.getX();
        int yDiff = tile.getY() - this.tile.getY() < 0 ? (tile.getY() - this.tile.getY()) * -1 : tile.getY() - this.tile.getY();

        if(xDiff != yDiff){
            return false;
        }

        Direction direction = Direction.STATIC.getDirection(this.tile, tile);
        for(int i = 1; i < xDiff; i++){
            Tile tileOnPath = this.tile.getBoard().getTile(this.tile.getX() + i * direction.getX()
                , this.tile.getY() + i * direction.getY());

            if(tileOnPath.getPiece() != null){
                return false;
            }
        }

        return true;
    }


}
