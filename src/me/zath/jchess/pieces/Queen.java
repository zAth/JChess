package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;
import me.zath.jchess.util.Direction;

public class Queen extends AbstractPiece {

    public Queen(Tile tile, Color color) {
        super(tile, color);
    }

    @Override
    public boolean isValidPath(Tile tile) {
        Direction direction = Direction.STATIC.getDirection(this.tile, tile);
        int xDiff = tile.getX() - this.tile.getX() < 0 ? (tile.getX() - this.tile.getX()) * -1 : tile.getX() - this.tile.getX();
        int yDiff = tile.getY() - this.tile.getY() < 0 ? (tile.getY() - this.tile.getY()) * -1 : tile.getY() - this.tile.getY();
        int diff = xDiff == 0 ? yDiff : xDiff;

        if (tile.getY() != this.tile.getY() && tile.getX() != this.tile.getX()) { // 1 axis must be equal to the current axis
            if(xDiff != yDiff){ // xdiff must be equal to ydiff to move diagonally
                return false;
            }
        }

        for(int i = 1; i < diff; i++){
            Tile tileOnPath = this.tile.getBoard().getTile(this.tile.getX() + i * direction.getX()
                , this.tile.getY() + i * direction.getY());

            if(tileOnPath.getPiece() != null){
                return false;
            }
        }

        return true;
    }

}
