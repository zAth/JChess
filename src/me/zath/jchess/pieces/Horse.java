package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;

public class Horse extends AbstractPiece {

    public Horse(Tile tile, Color color) {
        super(tile, color);
    }

    @Override
    public boolean isValidPath(Tile tile) {
        int xDiff = tile.getX() - this.tile.getX() < 0 ? (tile.getX() - this.tile.getX()) * -1 : tile.getX() - this.tile.getX();
        int yDiff = tile.getY() - this.tile.getY() < 0 ? (tile.getY() - this.tile.getY()) * -1 : tile.getY() - this.tile.getY();

        return xDiff != 3 && yDiff != 3 && xDiff + yDiff == 3;
    }

}
