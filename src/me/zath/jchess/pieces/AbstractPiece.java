package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.JChess;
import me.zath.jchess.board.Tile;
import me.zath.jchess.screen.GameScreen;

public abstract class AbstractPiece {

    protected Tile tile;
    protected Color color;

    public AbstractPiece(Tile tile, Color color) {
        this.tile = tile;
        this.tile.setPiece(this);
        this.color = color;
    }

    public Tile getTile() {
        return tile;
    }

    public Color getColor() {
        return color;
    }

    public boolean move(Tile tile) {
        if (tile == null) {
            return false;
        }
        if (this.tile == tile) {
            return false;
        }
        if (tile.getPiece() != null) {
            if (tile.getPiece().getColor() == this.getColor()) {
                return false;
            }
        }
        if (!isValidPath(tile)) {
            return false;
        }

        if(tile.getPiece() instanceof King){
            ((GameScreen) JChess.getJChess().getAbstractScreen()).setWinner(color);
            System.out.println(((GameScreen) JChess.getJChess().getAbstractScreen()).getWinner().name() + " won!");
            teleport(tile);
            return false;
        }

        teleport(tile);

        return true;
    }

    public void teleport(Tile tile){
        if(tile.getPiece() != null){
            ((GameScreen) JChess.getJChess().getAbstractScreen()).getFallenPieces().add(tile.getPiece());
        }

        this.tile.setPiece(null);
        this.tile = tile;
        this.tile.setPiece(this);
    }

    public abstract boolean isValidPath(Tile tile);

    @Override
    public String toString() {
        return "AbstractPiece{" +
            "type=" + getClass().getSimpleName() +
            ", color=" + color +
            '}';
    }

    public enum Color {
        WHITE, BLACK
    }

}
