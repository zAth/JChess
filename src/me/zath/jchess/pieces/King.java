package me.zath.jchess.pieces;
/*
 * JChess 
 * Created by zAth
 */

import me.zath.jchess.board.Tile;
import me.zath.jchess.util.Direction;

public class King extends AbstractPiece {

    private boolean moved;

    public King(Tile tile, Color color) {
        super(tile, color);
        moved = false;
    }

    @Override
    public boolean move(Tile tile) {
        int currentY = this.tile.getY();
        boolean superMoved = super.move(tile);
        if (superMoved) {
            moved = true;

            int yDiff = tile.getY() - currentY < 0 ? (tile.getY() - currentY) * -1 : tile.getY() - currentY;
            if(yDiff == 2) {
                Direction direction = Direction.STATIC.getDirection(this.tile.getBoard().getTile(tile.getX(), currentY), tile);
                int towerY = direction == Direction.NORTH ? 7 : 0;
                int towerX = color == Color.BLACK ? 0 : 7;
                Tile towerTile = this.tile.getBoard().getTile(towerX, towerY);
                Tower tower = (Tower) towerTile.getPiece();

                tower.teleport(this.tile.getBoard().getTile(
                    towerTile.getX(), tile.getY() + Direction.STATIC.getDirection(towerTile, tile).getY())
                );
            }
        }

        return superMoved;
    }

    @Override
    public boolean isValidPath(Tile tile) {
        int xDiff = tile.getX() - this.tile.getX() < 0 ? (tile.getX() - this.tile.getX()) * -1 : tile.getX() - this.tile.getX();
        int yDiff = tile.getY() - this.tile.getY() < 0 ? (tile.getY() - this.tile.getY()) * -1 : tile.getY() - this.tile.getY();

        if (xDiff > 1) {
            return false;
        }

        if (yDiff == 2) {
            if(xDiff != 0)
                return false;

            if(moved)
                return false;

            Direction direction = Direction.STATIC.getDirection(this.tile, tile);

            int towerY = direction == Direction.NORTH ? 7 : 0;
            int towerX = color == Color.BLACK ? 0 : 7;
            Tile towerTile = this.tile.getBoard().getTile(towerX, towerY);

            if(towerTile.getPiece() == null)
                return false;

            if(!(towerTile.getPiece() instanceof Tower))
                return false;

            Tower tower = (Tower) towerTile.getPiece();

            if(tower.hasMoved())
                return false;

            for(int i = 1; i < yDiff; i++){
                Tile tileOnPath = this.tile.getBoard().getTile(this.tile.getX()
                    , this.tile.getY() + i * direction.getY());

                if(tileOnPath.getPiece() != null){
                    return false;
                }
            }

            return true;
        }

        return !(xDiff > 1 || yDiff > 1);

    }

}
