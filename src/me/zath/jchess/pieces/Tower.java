package me.zath.jchess.pieces;
/*
 * JChess
 * Created by zAth
 */

import me.zath.jchess.board.Tile;
import me.zath.jchess.util.Direction;

public class Tower extends AbstractPiece {

    private boolean moved;

    public Tower(Tile tile, Color color) {
        super(tile, color);
        moved = false;
    }

    @Override
    public boolean move(Tile tile) {
        boolean superMoved = super.move(tile);
        if (superMoved)
            moved = true;

        return superMoved;
    }

    @Override
    public boolean isValidPath(Tile tile) {
        if (tile.getY() != this.tile.getY() && tile.getX() != this.tile.getX()) { // 1 axis must be equal to the current axis
            return false;
        }

        Direction direction = Direction.STATIC.getDirection(this.tile, tile);
        int xDiff = tile.getX() - this.tile.getX() < 0 ? (tile.getX() - this.tile.getX()) * -1 : tile.getX() - this.tile.getX();
        int yDiff = tile.getY() - this.tile.getY() < 0 ? (tile.getY() - this.tile.getY()) * -1 : tile.getY() - this.tile.getY();
        int diff = xDiff == 0 ? yDiff : xDiff;

        for (int i = 1; i < diff; i++) {
            Tile tileOnPath = this.tile.getBoard().getTile(this.tile.getX() + i * direction.getX()
                , this.tile.getY() + i * direction.getY());

            if (tileOnPath.getPiece() != null) {
                return false;
            }
        }

        return true;
    }

    public boolean hasMoved() {
        return moved;
    }

}
